package test2;

import java.util.Scanner;

public class PracticeArrays {
	
	public static void main(String[] args) {

		String[] name = {
				"Sahil", "Lorena", "Ana", "Andres", 
				"Nestor", "Gustavo", "Beatriz", "Lilibeth",
				"Sergio", "Camilo"};
		
		Boolean namecompare = false;
		int i;

		Scanner input = new Scanner(System.in);
		System.out.println("Please insert the name that you want to search");
		String searchName = input.nextLine();
		
		System.out.println("Searching " + searchName + " ...");
				
		for (i=0; i<name.length; i++) {
			if (name[i].equalsIgnoreCase(searchName)) {
				namecompare = true;
				break;
			}
		}
		
		if (namecompare == true) {
		System.out.println("Match found with " + searchName + " in position " + i);
		} else {
		System.out.println("Match not found for " + "'" + searchName + "'");
		}
	}
}
